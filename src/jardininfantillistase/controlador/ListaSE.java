/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jardininfantillistase.controlador;

import jardininfantillistase.modelo.Infante;
import jardininfantillistase.modelo.Nodo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carloaiza
 */
public class ListaSE {

    public Nodo cabeza;

    public ListaSE() {
    }
    
       public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    public int contarNodos() {
        if (cabeza != null) {
            int cont = 0;
            Nodo temp = cabeza;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();

            }
            return cont;
        }
        return 0;
    }

    public String listarNodos() {
        if (cabeza != null) {
            String listado = "";
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();

            }
            return listado;
        }
        return "No hay datos";
    }
    
    public List<Infante> listarNodosInfantes() {
        if (cabeza != null) {
            List<Infante> listado = new ArrayList<>();
            Nodo temp = cabeza;
            while (temp != null) {
                listado.add(temp.getDato());
                temp = temp.getSiguiente();

            }
            return listado;
        }
        return null;
    }
    
    
    public void adicionarNodo(Infante dato)
    {
        if(cabeza!=null)
        {
            Nodo temp= cabeza;
            while(temp.getSiguiente()!=null)
            {
                temp= temp.getSiguiente();
            }   
            temp.setSiguiente(new Nodo(dato));
        }   
        else
        {
           cabeza = new Nodo(dato);
        }    
    }
    
    
    public void adicionarNodoAlInicio(Infante dato)
    {
        if(cabeza!=null)
        {
            Nodo nuevo= new Nodo(dato);
            nuevo.setSiguiente(cabeza);
            cabeza= nuevo;            
        }   
        else
        {
           cabeza = new Nodo(dato);
        }
    }

    
    public void invertirLista()
    {
        Nodo temp=cabeza;
        ListaSE listaCopia= new ListaSE();
        
        while(temp!=null)
        {
            listaCopia.adicionarNodoAlInicio(temp.getDato());            
            temp= temp.getSiguiente();
        }
        cabeza= listaCopia.getCabeza();
        
        
    }
    
    public boolean eliminarNodo(Infante datoEliminar)
    {
        if(cabeza!=null)
        {
            if(cabeza.getDato().getNombre().equals(datoEliminar.getNombre()))
            {
                cabeza= cabeza.getSiguiente();
                return true;
            }
            else
            {
                Nodo temp=cabeza;
                while(temp!=null)
                {
                    if(temp.getSiguiente()!=null)
                    {
                        if(temp.getSiguiente().getDato().getNombre().equals(datoEliminar.getNombre()))
                        {
                            temp.setSiguiente(temp.getSiguiente().getSiguiente());
                            return true;
                        }
                    }
                    temp=temp.getSiguiente();
                }
                
            }
        }
        
        return false;
    }
    
    public boolean eliminarNodoXPosicion(int pos) {

        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 0;
            if (pos > 0 && pos <= contarNodos()) {
                while (temp != null) {
                    cont++;
                    if (cont == pos) {
                        eliminarNodo(temp.getDato());
                        return true;
                    } else {
                        temp = temp.getSiguiente();
                    }
                }
            }
            return false;
        }

        return false;
    }
    
    public Nodo encontrarUltimo(){
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {                
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }
    
}
