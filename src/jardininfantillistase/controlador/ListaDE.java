/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jardininfantillistase.controlador;

import jardininfantillistase.modelo.Infante;
import jardininfantillistase.modelo.NodoDE;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SALGADO
 */
public class ListaDE {

    private NodoDE cabeza;

    public ListaDE() {
    }

    public NodoDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoDE cabeza) {
        this.cabeza = cabeza;
    }

    //---------------------
    //METODOS
    //--------------------
    public void adicionarNodo(Infante dato) {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            temp.setSiguiente(new NodoDE(dato));
            temp.getSiguiente().setAnterior(temp);
        } else {
            cabeza = new NodoDE(dato);
        }
    }

    public NodoDE encontrarUltimo() {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }

    public int contarNodos() {
        if (cabeza != null) {
            int contador = 0;
            NodoDE temp = cabeza;
            while (temp != null) {
                contador++;
                temp = temp.getSiguiente();
            }
            return contador;
        } else {
            return 0;
        }
    }

    public boolean eliminarNodo(Infante datoEliminar) {
        if (cabeza != null) {
            if (cabeza.getDato().getNombre().equals(datoEliminar.getNombre())) {
                cabeza = cabeza.getSiguiente();
                return true;
            } else {
                NodoDE temp = cabeza;
                while (temp != null) {
                    if (temp.getSiguiente() != null) {
                        if (temp.getSiguiente().getDato().getNombre().equals(datoEliminar.getNombre())) {
                            temp.setSiguiente(temp.getSiguiente().getSiguiente());
                            if (temp.getSiguiente() != null) {
                                temp.getSiguiente().setAnterior(temp);
                            }

                            return true;
                        }
                    }
                    temp = temp.getSiguiente();
                }

            }
        }

        return false;
    }

    public boolean eliminarDondeEstoyParado(Infante datoEliminar) {
        if (cabeza != null) {
            if (cabeza.getDato().getNombre().equals(datoEliminar.getNombre())) {
                cabeza = cabeza.getSiguiente();
                if (cabeza != null) {
                    cabeza.setAnterior(null);
                    return true;
                }
            } else {
                NodoDE temp = cabeza;
                while (temp != null) {
                    if (temp.getDato().getNombre().equals(datoEliminar.getNombre())) {

                        temp.getAnterior().setSiguiente(temp.getSiguiente());

                        if (temp.getSiguiente() != null) {
                            temp.getSiguiente().setAnterior(temp.getAnterior());
                        }

                        return true;
                    }
                    temp = temp.getSiguiente();
                }
            }

        }
        return false;
    }

    public boolean eliminarNodoXPosicion(int pos) {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            int cont = 0;
            if (pos > 0 && pos <= contarNodos()) {
                while (temp != null) {
                    cont++;
                    if (cont == pos) {
                        eliminarNodo(temp.getDato());
                        return true;
                    } else {
                        temp = temp.getSiguiente();
                    }
                }
            }
            return false;
        }

        return false;
    }

    public String listarNodos() {

        if (cabeza != null) {
            String listado = "";
            NodoDE temp = cabeza;
            while (temp != null) {
                listado = listado + temp.getDato();
            }
            return listado;
        } else {

            return "No hay niños";
        }
    }

    public List<Infante> listarNodosInfante() {
        if (cabeza != null) {
            List<Infante> infantes = new ArrayList<>();
            NodoDE temp = cabeza;
            while (temp != null) {
                infantes.add(temp.getDato());
                temp.getSiguiente();
            }
            return infantes;
        } else {
            return null;
        }

    }

    public void adicionarNodoAlInicio(Infante dato) {
        if (cabeza != null) {
            NodoDE nuevo = new NodoDE(dato);
            nuevo.setSiguiente(cabeza);
            cabeza.setAnterior(nuevo);
            cabeza = nuevo;
        } else {
            cabeza = new NodoDE(dato);
        }
    }

    public void invertirLista() {
        NodoDE temp = cabeza;
        ListaDE listaCopia = new ListaDE();
        while (temp != null) {
            listaCopia.adicionarNodoAlInicio(temp.getDato());
            temp = temp.getSiguiente();
        }
        cabeza = listaCopia.getCabeza();
    }

    public void listarInfanteMasculino() {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            ListaDE listaCopia = new ListaDE();
            while (temp != null) {
                if (temp.getDato().getGenero() == 'M') {
                    listaCopia.adicionarNodo(temp.getDato());
                    temp = temp.getSiguiente();
                } else {
                    temp = temp.getSiguiente();
                }
            }
            cabeza = listaCopia.getCabeza();
        }
    }

    public void listarInfanteFemenino() {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            ListaDE listaCopia = new ListaDE();
            while (temp != null) {
                if (temp.getDato().getGenero() == 'F') {
                    listaCopia.adicionarNodo(temp.getDato());
                    temp = temp.getSiguiente();
                } else {
                    temp = temp.getSiguiente();
                }
            }
            cabeza = listaCopia.getCabeza();
        }
    }

    public void paresImpares() {
        int pos = 0;
        if (contarNodos() % 2 != 0) {
            pos = contarNodos() / 2 + 1;
            eliminarNodoXPosicion(pos);
        } else {
            NodoDE temp = cabeza;
            ListaDE listaCopia = new ListaDE();
            while (temp.getSiguiente() != null) {
                listaCopia.adicionarNodo(temp.getDato());
                temp = temp.getSiguiente();
            }
            listaCopia.adicionarNodoAlInicio(temp.getDato());
            listaCopia.setCabeza(temp);
            NodoDE tempListaCopia = listaCopia.getCabeza();

            while (tempListaCopia.getSiguiente() != null) {
                tempListaCopia = tempListaCopia.getSiguiente();
            }
            tempListaCopia.setSiguiente(cabeza);
            cabeza = listaCopia.getCabeza();

        }
    }

    public boolean listarMenoresEdad3() {
        if (cabeza != null) {
            NodoDE temp = cabeza;
            ListaDE listaCopia = new ListaDE();
            while (temp != null) {
                if (temp.getDato().getEdad() < 3) {
                    listaCopia.adicionarNodoAlInicio(temp.getDato());
                    temp = temp.getSiguiente();
                } else {
                    listaCopia.adicionarNodo(temp.getDato());
                    temp = temp.getSiguiente();
                }
            }
            cabeza = listaCopia.getCabeza();
            return true;
        }
        return false;
    }

}
