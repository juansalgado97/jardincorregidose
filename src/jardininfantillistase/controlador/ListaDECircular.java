/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jardininfantillistase.controlador;

import jardininfantillistase.modelo.Infante;
import jardininfantillistase.modelo.NodoDE;

/**
 *
 * @author SALGADO
 */
public class ListaDECircular {

    private NodoDE cabeza;

    public ListaDECircular() {
    }

    public NodoDE getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoDE cabeza) {
        this.cabeza = cabeza;
    }

    //---------------------
    //METODOS
    //--------------------
    public boolean adicionarNodo(Infante dato, boolean esCabeza) {
        if (cabeza != null) {
            NodoDE nuevo = new NodoDE(dato);
            NodoDE ultimo = cabeza.getAnterior();

            cabeza.setAnterior(nuevo);
            nuevo.setSiguiente(cabeza);
            nuevo.setAnterior(ultimo);
            ultimo.setSiguiente(nuevo);

            if (esCabeza) {
                cabeza = nuevo;
                return true;
            }
            return true;
        }
        cabeza = new NodoDE(dato);
        cabeza.setSiguiente(cabeza);
        cabeza.setAnterior(cabeza);
        return true;
    }

    public NodoDE encontrarElUltimo() {
        if (cabeza != null) 
        {
            return cabeza.getAnterior();
        }
        return null;
    }

    public int contarNodos() {
        if (cabeza != null) 
        {
            int contador = 1;
            NodoDE temp = cabeza;
            
            while (temp.getSiguiente() != cabeza) 
            {
                contador++;
                temp = temp.getSiguiente();
            }
            return contador;
        } 
        else 
        {
            return 0;
        }
    }

    public boolean eliminarDondeEstoyParado(Infante datoEliminar) {
        if (cabeza != null) {
            if (cabeza.getDato().getNombre().equals(datoEliminar.getNombre())) {
                if (cabeza.getSiguiente() != cabeza) {
                    cabeza.getAnterior().setSiguiente(cabeza.getSiguiente());
                    cabeza.getSiguiente().setAnterior(cabeza.getAnterior());
                    cabeza = cabeza.getSiguiente();
                    return true;
                } else {
                    cabeza = null;
                    return true;
                }
            } 
            else 
            {
                NodoDE temp = cabeza;
                while (temp != null) {
                    if (temp.getDato().getNombre().equals(datoEliminar.getNombre())) {

                        temp.getAnterior().setSiguiente(temp.getSiguiente());
                        temp.getSiguiente().setAnterior(temp.getAnterior());
                        return true;
                    }
                    temp = temp.getSiguiente();
                }
            }
            return false;
        }
        return false;
    }
    
    public int contarNodoFemenino() {
        if (cabeza != null) {
            int cont = 0;
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != cabeza) {
                if (temp.getDato().getGenero() == 'F') {
                    cont++;
                }
                temp = temp.getSiguiente();
            }
            if (temp.getDato().getGenero() == 'F') {
                cont++;
            }
            return cont;
        } else {
            return 0;
        }
    }
    
    public int contarNodoMasculino() {
        if (cabeza != null) {
            int cont = 0;
            NodoDE temp = cabeza;
            while (temp.getSiguiente() != cabeza) {
                if (temp.getDato().getGenero() == 'M') {
                    cont++;
                }
                temp = temp.getSiguiente();
            }
            if (temp.getDato().getGenero() == 'M') {
                cont++;
            }
            return cont;
        } else {
            return 0;
        }
    }
    
    public Infante jugarDerecha(int pos)
    {
        if (cabeza != null) 
        {
            NodoDE temp=cabeza;
            Infante infanteALatabla;
            int contador=0;
            
            while(temp!=null){
                contador++;
                if (contador == pos) {
                    infanteALatabla = temp.getDato();
                    eliminarDondeEstoyParado(temp.getDato());
                    return infanteALatabla;
                }
                temp = temp.getSiguiente();
            }
            return null;
        }
        return null;
    }
    
      public Infante jugarIzquierda(int pos)
    {
        if (cabeza != null) 
        {
            NodoDE temp=cabeza;
            Infante infanteALatabla;
            int contador=0;
            
            while(temp!=null){
                contador++;
                if (contador == pos) {
                    infanteALatabla = temp.getDato();
                    eliminarDondeEstoyParado(temp.getDato());
                    return infanteALatabla;
                }
                temp = temp.getAnterior();
            }
            return null;
        }
        return null;
    }
    
    public int devolverNumeroAlAzar(){
        int pos = (int) (Math.random() * 300);
        return  pos;
    }

}
